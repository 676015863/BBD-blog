# BBD-blog
### BBD前端团队博客系统 
> 服务端 nodejs + mysql + koa + sequelize 

> web前端 react + mobx

```
=== node-server ===
npm install
npm start
localhost:7770

=== node-web ===
npm install
npm start
localhost:7777

hello
```

> 开发前端时启动 node-web，代理node-server api接口